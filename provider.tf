terraform {
  backend "s3" {
    bucket         = "s3statebackendiris"
    dynamodb_table = "state-lock"
    key            = "global/mystatefile/terraform.tfstate"
    region         = "eu-west-3"
    encrypt        = true
  }
}