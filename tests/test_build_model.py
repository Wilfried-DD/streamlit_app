"""
Test on building model
"""
import unittest
import os

from backend.build_model import train_and_save_model

import pandas as pd

class TestModelTraining(unittest.TestCase):

    def test_csv_columns(self):
        # todo à déplacer Ne doit pas ètre ici
        # Check if the required columns are present in the CSV file
        df = pd.read_csv('dataset/Iris.csv')
        required_columns = ['SepalLengthCm', 'SepalWidthCm', 'PetalLengthCm', 'PetalWidthCm', 'Species']
        self.assertTrue(all(col in df.columns for col in required_columns))

    def test_pickle_file_created(self):
        # Check if a classification pickle file is created
        accuracy = train_and_save_model()
        self.assertTrue(os.path.exists("pic_model/classification_model.pkl"))

    def test_model_training(self):
        accuracy = train_and_save_model()
        self.assertTrue(accuracy >= 0 and accuracy <= 1)

if __name__ == '__main__':
    unittest.main()