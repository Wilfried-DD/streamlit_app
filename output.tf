output "iris-alb-dns-name" {
  value = aws_lb.iris-alb.dns_name
}
output "iris-instance-public-ip" {
  value = aws_instance.iris-instance.public_ip
}