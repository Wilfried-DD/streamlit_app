provider "aws" {
  region = "eu-west-3"
}

# Create a VPC
resource "aws_vpc" "iris-vpc" {
  cidr_block           = "10.1.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "iris-vpc"
  }
}

# Create an internet gateway and attach to VPC
resource "aws_internet_gateway" "iris-igw" {
  vpc_id = aws_vpc.iris-vpc.id

  tags = {
    Name = "iris-igw"
  }
}
# Create public subnet for ALB
resource "aws_subnet" "pub_sub" {
  vpc_id            = aws_vpc.iris-vpc.id
  cidr_block        = "10.1.2.0/24"
  availability_zone = "eu-west-3a" # Specify a different availability zone
  map_public_ip_on_launch = true
  tags = {
    Name = "pub_sub"
  }
}
# Create public subnet for ALB
resource "aws_subnet" "pub_sub_2" {
  vpc_id            = aws_vpc.iris-vpc.id
  cidr_block        = "10.1.3.0/24"
  availability_zone = "eu-west-3b" # Specify a different availability zone

  tags = {
    Name = "pub_sub_2"
  }
}
# Create private subnet for EC2 instance
resource "aws_subnet" "priv_sub" {
  vpc_id            = aws_vpc.iris-vpc.id
  cidr_block        = "10.1.1.0/24"
  availability_zone = "eu-west-3a"

  tags = {
    Name = "priv_sub"
  }
}




# Create an Elastic IP address for the NAT gateway
resource "aws_eip" "iris-eip" {
  tags = {
    Name = "iris-eip"
  }
}

# Create a NAT gateway and attach to the public subnet
resource "aws_nat_gateway" "iris-nat-gw" {
  allocation_id = aws_eip.iris-eip.id
  subnet_id     = aws_subnet.pub_sub.id

  tags = {
    Name = "iris-nat-gw"
  }

  depends_on = [aws_internet_gateway.iris-igw]
}

# Create a route table for the public subnet
resource "aws_route_table" "iris-pub-route-table" {
  vpc_id = aws_vpc.iris-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.iris-igw.id
  }

  tags = {
    Name = "iris-pub-route-table"
  }
}

# Create the route table association to the public subnet
resource "aws_route_table_association" "iris-pub-route-association" {
  subnet_id      = aws_subnet.pub_sub.id
  route_table_id = aws_route_table.iris-pub-route-table.id
}

# Create a route table for the private subnet
resource "aws_route_table" "iris-priv-route-table" {
  vpc_id = aws_vpc.iris-vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.iris-nat-gw.id
  }

  tags = {
    Name = "iris-priv-route-table"
  }
}

# Create the route table association to the private subnet
resource "aws_route_table_association" "iris-priv-route-association" {
  subnet_id      = aws_subnet.priv_sub.id
  route_table_id = aws_route_table.iris-priv-route-table.id
}

# Create main route table association
resource "aws_main_route_table_association" "iris-main-route-association" {
  vpc_id         = aws_vpc.iris-vpc.id
  route_table_id = aws_route_table.iris-pub-route-table.id
}
# Create an EC2 instance
resource "aws_instance" "iris-instance" {
  ami                         = "ami-089c89a80285075f7"
  instance_type               = "t2.micro"
  key_name                    = "iris-key"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.iris-sg-instances.id]
  # security_groups = [aws_security_group.iris-sg-instances.name]
  subnet_id = aws_subnet.priv_sub.id
  user_data = <<-EOF
    #!/bin/bash
    set -ex
    sudo yum update -y
    sudo yum install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
  EOF

  tags = {
    Name = "iris-instance"
  }
}

# Create a security group for instances
resource "aws_security_group" "iris-sg-instances" {
  name        = "iris-sg-instances"
  description = "Security group for instances in iris project"
  vpc_id      = aws_vpc.iris-vpc.id

  ingress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.iris-vpc.cidr_block]
  }
  ingress {
    description = "Streamlit application port"
    from_port   = 8501
    to_port     = 8501
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.iris-vpc.cidr_block]
  }
  ingress {
    description = "SSH from everywhere to my application"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "iris-sg-instances"
  }
}

# Create an ALB target group
resource "aws_lb_target_group" "iris-alb-target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
  name        = "iris-alb-target-group"
  port        = 8501
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.iris-vpc.id
}

# Create an Application Load Balancer (ALB)
resource "aws_lb" "iris-alb" {
  name               = "iris-alb"
  internal           = false
  load_balancer_type = "application"
  ip_address_type    = "ipv4"
  subnets            = [aws_subnet.pub_sub.id, aws_subnet.pub_sub_2.id]

  enable_deletion_protection = true

  tags = {
    Name = "iris-alb"
  }
}

# Create an ALB listener
resource "aws_lb_listener" "iris-alb-listener" {
  load_balancer_arn = aws_lb.iris-alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.iris-alb-target-group.arn
    type             = "forward"
  }
}

# Attach instances to the ALB target group
resource "aws_lb_target_group_attachment" "iris-attach-alb-tg" {
  target_group_arn = aws_lb_target_group.iris-alb-target-group.arn
  target_id        = aws_instance.iris-instance.id
}