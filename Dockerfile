# Use Python 3.10 base image
FROM python:3.10-slim

RUN pip install poetry  
RUN mkdir -p /app  
COPY . /app

WORKDIR /app

RUN poetry install
RUN poetry run python -m backend.build_model

# Expose port 8501
EXPOSE 8501

CMD ["poetry", "run", "streamlit", "run", "--server.port", "8501", "frontend_streamlit/streamlit_iris.py"]