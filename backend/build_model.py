"""
File to build and save the model for futur prediction on petal 
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
import joblib

def train_and_save_model():
    """
    Build and save the model
    """
    # Load the dataset
    df = pd.read_csv('dataset/Iris.csv')
    df.drop(['Id'], axis=1, inplace=True)

    df.loc[df.Species == 'Iris-setosa', 'Species'] = 1
    df.loc[df.Species == 'Iris-versicolor', 'Species'] = 2
    df.loc[df.Species == 'Iris-virginica', 'Species'] = 3

    # Split the dataset into features (X) and target (y)
    X = df[['SepalLengthCm', 'SepalWidthCm', 'PetalLengthCm', 'PetalWidthCm']]
    y = df['Species'].astype(int)
    y = np.array(y).reshape((len(y), ))

    # Split the dataset into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    # Train a Logistic Regression model
    LR = LogisticRegression()
    LR.fit(X_train, y_train)

    # Make predictions on the testing set
    pred = LR.predict(X_test)

    # Calculate accuracy
    accuracy = accuracy_score(pred, y_test)

    # Save the trained model
    joblib.dump(LR, "pic_model/classification_model.pkl")

    return accuracy

if __name__ == "__main__":
    train_and_save_model()